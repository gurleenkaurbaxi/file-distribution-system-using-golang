# File Distribution System using Golang

Problem:
Centralized servers have long been the backbone of data storage and management, but they present significant limitations, including scalability issues, network congestion, and a single point of failure. As data volumes grow, the need for a more scalable, reliable, and efficient system becomes paramount.

Solution:
This project addresses these challenges by implementing a Distributed File System (DFS). The DFS distributes data storage across multiple servers, enhancing scalability, reducing network congestion, and eliminating single points of failure. Data is replicated across servers, ensuring continuous availability even in the event of server failures. The system effectively balances data requests across the network, optimizing performance.

Project Workflow:
The project follows a systematic workflow designed to maximize efficiency and ensure data integrity. Below is a high-level overview of how the system operates:
1. Initialization: The system begins by setting up a network of servers. Each server is equipped to store and manage a portion of the data.
2. Data Distribution: When a file is uploaded to the system, it is split into smaller parts and distributed across multiple servers. This ensures that even if one server fails, the file can still be reconstructed from the remaining parts.
3. Data Retrieval: When a file is requested, the system gathers the relevant parts from different servers, reassembles them, and delivers the complete file to the user.
4. Error Handling: The system constantly monitors server health. If a server goes down, the system automatically redistributes the data to maintain redundancy and reliability.
(Check PPT Deck)

Dependencies:
-Go version 1.19 or higher. 
-Makefile for build automation, and GitLab for version control.

File Structure:
The repository is organized as follows:
/cmd: Main executable files (e.g., main.go, server.go).
/pkg: Packages for storage, encryption, and server communication.
/test: Unit and integration tests.
Makefile: Script for automating builds and execution.

Testing:
Several tests are implemented to verify system integrity:
1. Store Tests: Ensures correct file storage and retrieval.
2. Crypto Tests: Validates encryption and decryption processes.
3. make run main.go
4. make run server.go

Key challenges encountered include:
1. Network Communication: Ensuring reliable server communication.
2. Security: Implementing robust data encryption.
3. Data Sharding: Efficiently partitioning data.
4. Testing: Comprehensive testing for reliability.

Potential enhancements include:
1. Advanced Security: Incorporating more sophisticated security protocols.
2. Machine Learning: Integrating ML for dynamic optimization.
3. Data Analytics: Developing advanced analytics tools.
4. Testing Improvements: Expanding automated testing frameworks.

References: Youtube : Anthony ggg , Free Code Camp

Contact:
Gurleen Kaur Baxi
Email: gurleen3825.beai23@chitkara.edu.in
Chitkara University, WE Cohort - 6